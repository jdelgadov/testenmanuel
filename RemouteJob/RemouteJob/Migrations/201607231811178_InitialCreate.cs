namespace RemouteJob.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.SellersModels",
                c => new
                    {
                        IdSeller = c.Int(nullable: false, identity: true),
                        FullName = c.String(),
                        IdentityID = c.String(),
                    })
                .PrimaryKey(t => t.IdSeller);
            
            CreateTable(
                "dbo.ClientsModels",
                c => new
                    {
                        IdClient = c.Int(nullable: false, identity: true),
                        FullName = c.String(),
                        IdentityID = c.String(),
                        Seller_IdSeller = c.Int(),
                    })
                .PrimaryKey(t => t.IdClient)
                .ForeignKey("dbo.SellersModels", t => t.Seller_IdSeller)
                .Index(t => t.Seller_IdSeller);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ClientsModels", "Seller_IdSeller", "dbo.SellersModels");
            DropIndex("dbo.ClientsModels", new[] { "Seller_IdSeller" });
            DropTable("dbo.ClientsModels");
            DropTable("dbo.SellersModels");
        }
    }
}
