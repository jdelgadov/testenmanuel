namespace RemouteJob.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Phonecontact : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ClientsModels", "PhoneContact", c => c.String());
            DropColumn("dbo.ClientsModels", "PhoneNumber");
        }
        
        public override void Down()
        {
            AddColumn("dbo.ClientsModels", "PhoneNumber", c => c.String());
            DropColumn("dbo.ClientsModels", "PhoneContact");
        }
    }
}
