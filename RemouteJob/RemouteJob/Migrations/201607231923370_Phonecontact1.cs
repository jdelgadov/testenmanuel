namespace RemouteJob.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Phonecontact1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.SellersModels", "PhoneContact", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.SellersModels", "PhoneContact");
        }
    }
}
