namespace RemouteJob.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Address : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ClientsModels", "Address", c => c.String());
            AddColumn("dbo.ClientsModels", "PhoneNumber", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.ClientsModels", "PhoneNumber");
            DropColumn("dbo.ClientsModels", "Address");
        }
    }
}
