namespace RemouteJob.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class IDClient : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.SellersModels", "FullName", c => c.String(nullable: false));
            AlterColumn("dbo.SellersModels", "IdentityID", c => c.String(nullable: false));
            AlterColumn("dbo.SellersModels", "PhoneContact", c => c.String(nullable: false));
            AlterColumn("dbo.SellersModels", "Department", c => c.String(nullable: false));
            AlterColumn("dbo.ClientsModels", "FullName", c => c.String(nullable: false));
            AlterColumn("dbo.ClientsModels", "IdentityID", c => c.String(nullable: false));
            AlterColumn("dbo.ClientsModels", "PhoneContact", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.ClientsModels", "PhoneContact", c => c.String());
            AlterColumn("dbo.ClientsModels", "IdentityID", c => c.String());
            AlterColumn("dbo.ClientsModels", "FullName", c => c.String());
            AlterColumn("dbo.SellersModels", "Department", c => c.String());
            AlterColumn("dbo.SellersModels", "PhoneContact", c => c.String());
            AlterColumn("dbo.SellersModels", "IdentityID", c => c.String());
            AlterColumn("dbo.SellersModels", "FullName", c => c.String());
        }
    }
}
