namespace RemouteJob.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Department : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.SellersModels", "Department", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.SellersModels", "Department");
        }
    }
}
