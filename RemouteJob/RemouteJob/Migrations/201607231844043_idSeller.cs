namespace RemouteJob.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class idSeller : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.ClientsModels", "Seller_IdSeller", "dbo.SellersModels");
            DropIndex("dbo.ClientsModels", new[] { "Seller_IdSeller" });
            RenameColumn(table: "dbo.ClientsModels", name: "Seller_IdSeller", newName: "idSeller");
            AlterColumn("dbo.ClientsModels", "idSeller", c => c.Int(nullable: false));
            CreateIndex("dbo.ClientsModels", "idSeller");
            AddForeignKey("dbo.ClientsModels", "idSeller", "dbo.SellersModels", "IdSeller", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ClientsModels", "idSeller", "dbo.SellersModels");
            DropIndex("dbo.ClientsModels", new[] { "idSeller" });
            AlterColumn("dbo.ClientsModels", "idSeller", c => c.Int());
            RenameColumn(table: "dbo.ClientsModels", name: "idSeller", newName: "Seller_IdSeller");
            CreateIndex("dbo.ClientsModels", "Seller_IdSeller");
            AddForeignKey("dbo.ClientsModels", "Seller_IdSeller", "dbo.SellersModels", "IdSeller");
        }
    }
}
