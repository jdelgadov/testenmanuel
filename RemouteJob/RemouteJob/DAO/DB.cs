﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using RemouteJob.Models;

namespace RemouteJob.DAO
{
    public class DB:DbContext
    {
        public DbSet<SellersModels> Blogs { get; set; }
        public DbSet<ClientsModels> Posts { get; set; }
    }
}
