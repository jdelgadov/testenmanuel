﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RemouteJob.Models
{
    public class SellersModels
    {
        [Key]
       
        public int IdSeller { get; set; }
        [Required(ErrorMessage = "El nombre es requerido")]
        [Display(Name = "Nombre del vendedor")]
        public string FullName { get; set; }
        [Display(Name = "Cedula")]
        [Required(ErrorMessage = "La cedula es requerido")]
        public string IdentityID { get; set; }
        [Display(Name = "Numero de contacto")]
        [Required(ErrorMessage = "El telefono es requerido")]
        [DataType(DataType.PhoneNumber)]
        public string PhoneContact { get; set; }
        [Required(ErrorMessage = "El departamento al que pertences es requerido")]
        [Display(Name = "Departamento al que pertenece")]
        public string Department { get; set; }
        public virtual List<ClientsModels> clients{get;set;}
    }
}
