﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RemouteJob.Models
{
    public class ClientsModels
    {
        [Key]
        public int IdClient { get; set; }
        [Required(ErrorMessage = "El nombre es requerido")]
        [Display(Name = "Nombre Completo")]
        public string FullName { get; set; }
        [Display(Name = "Cedula")]
        [Required(ErrorMessage = "La cedula es requerido")]
        
        public string IdentityID { get; set; }
        [Display(Name = "Direccion")]
       
        public string Address { get; set; }
        [Required(ErrorMessage = "El numero de contacto es requerido")]
        [Display(Name = "Numero de contacto")]
        [DataType(DataType.PhoneNumber)]
      
        public string PhoneContact{ get; set; }
        [Display(Name = "Vendedor")]
        [Required(ErrorMessage = "Debe seleccionar un vendedor")]
        public int idSeller { get; set; }
        public virtual SellersModels Seller { get; set; }
    }
}
