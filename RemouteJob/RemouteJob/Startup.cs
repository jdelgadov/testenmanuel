﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(RemouteJob.Startup))]
namespace RemouteJob
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
