﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RemouteJob.DAO;
using RemouteJob.Models;

namespace RemouteJob.Controllers
{
    public class ClientsController : Controller
    {
        private DB db = new DB();

        // GET: Clients
        public ActionResult Index()
        {
            var posts = db.Posts.Include(c => c.Seller);
            return View(posts.ToList());
        }

        // GET: Clients/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ClientsModels clientsModels = db.Posts.Find(id);
            if (clientsModels == null)
            {
                return HttpNotFound();
            }
            return View(clientsModels);
        }

        // GET: Clients/Create
        public ActionResult Create()
        {
            ViewBag.idSeller = new SelectList(db.Blogs, "IdSeller", "FullName");
            return View();
        }

        // POST: Clients/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "IdClient,FullName,IdentityID,Address,PhoneContact,idSeller")] ClientsModels clientsModels)
        {
            if (ModelState.IsValid)
            {
                db.Posts.Add(clientsModels);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.idSeller = new SelectList(db.Blogs, "IdSeller", "FullName", clientsModels.idSeller);
            return View(clientsModels);
        }

        // GET: Clients/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ClientsModels clientsModels = db.Posts.Find(id);
            if (clientsModels == null)
            {
                return HttpNotFound();
            }
            ViewBag.idSeller = new SelectList(db.Blogs, "IdSeller", "FullName", clientsModels.idSeller);
            return View(clientsModels);
        }

        // POST: Clients/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "IdClient,FullName,IdentityID,Address,PhoneContact,idSeller")] ClientsModels clientsModels)
        {
            if (ModelState.IsValid)
            {
                db.Entry(clientsModels).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.idSeller = new SelectList(db.Blogs, "IdSeller", "FullName", clientsModels.idSeller);
            return View(clientsModels);
        }

        // GET: Clients/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ClientsModels clientsModels = db.Posts.Find(id);
            if (clientsModels == null)
            {
                return HttpNotFound();
            }
            return View(clientsModels);
        }

        // POST: Clients/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ClientsModels clientsModels = db.Posts.Find(id);
            db.Posts.Remove(clientsModels);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
