﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RemouteJob.DAO;
using RemouteJob.Models;

namespace RemouteJob.Controllers
{
    public class SellersController : Controller
    {
        private DB db = new DB();

        // GET: Sellers
        public ActionResult Index()
        {
            return View(db.Blogs.ToList());
        }

        // GET: Sellers/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SellersModels sellersModels = db.Blogs.Find(id);
            if (sellersModels == null)
            {
                return HttpNotFound();
            }
            return View(sellersModels);
        }

        // GET: Sellers/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Sellers/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "IdSeller,FullName,IdentityID,PhoneContact,Department")] SellersModels sellersModels)
        {
            if (ModelState.IsValid)
            {
                db.Blogs.Add(sellersModels);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(sellersModels);
        }

        // GET: Sellers/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SellersModels sellersModels = db.Blogs.Find(id);
            if (sellersModels == null)
            {
                return HttpNotFound();
            }
            return View(sellersModels);
        }

        // POST: Sellers/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "IdSeller,FullName,IdentityID,PhoneContact,Department")] SellersModels sellersModels)
        {
            if (ModelState.IsValid)
            {
                db.Entry(sellersModels).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(sellersModels);
        }

        // GET: Sellers/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SellersModels sellersModels = db.Blogs.Find(id);
            if (sellersModels == null)
            {
                return HttpNotFound();
            }
            return View(sellersModels);
        }

        // POST: Sellers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            SellersModels sellersModels = db.Blogs.Find(id);
            db.Blogs.Remove(sellersModels);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
